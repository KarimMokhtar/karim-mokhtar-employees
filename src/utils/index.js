const calcOverlap = (emp1date1, emp1date2, emp2date1, emp2date2) => {
  const startDate1 = new Date(emp1date1);
  const endDate1 = emp1date2 === "NULL" ? new Date() : new Date(emp1date2);
  const startDate2 = new Date(emp2date1);
  const endDate2 = emp2date2 === "NULL" ? new Date() : new Date(emp2date2);

  const start = startDate1 < startDate2 ? startDate2 : startDate1;
  const end = endDate1 < endDate2 ? endDate1 : endDate2;

  if (end >= start) {
    const diffTime = Math.abs(end - start);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
  }
  // in case there is no overlap
  return 0;
};

const compareEmp = projects => {
  const keys = Object.keys(projects);
  let matches = [];
  keys.forEach(ele => {
    let res = 0;
    const currMatch = { i: 0, j: 0 };
    if (ele !== 'undefined') {
      for (let i = 0; i < projects[ele].length; ++i) {
        for (let j = i + 1; j < projects[ele].length; ++j) {
          const overlap = calcOverlap(
            projects[ele][i].dateFrom,
            projects[ele][i].dateTo,
            projects[ele][j].dateFrom,
            projects[ele][j].dateTo
          );
          if (overlap > res) {
            res = overlap;
            currMatch.i = i;
            currMatch.j = j;
          }
        }
      }
      if (res !== 0) matches.push([projects[ele][currMatch.i].empId, projects[ele][currMatch.j].empId, ele, res]);
    }
  });
  return matches
};

export const modifyData = records => {
  const projects = {};
  records.map(rec => {
    const singleRecord = rec.split(",");
    const projId = singleRecord[1]?.trim();
    const record = {
      empId: singleRecord[0]?.trim(),
      projId: projId,
      dateFrom: singleRecord[2]?.trim(),
      dateTo: singleRecord[3]?.trim(),
    };
    if (projects[projId]) projects[projId] = [...projects[projId], { ...record }];
    else projects[projId] = [{ ...record }];
    return record;
  });

  const res = compareEmp(projects);
  return res;
};
