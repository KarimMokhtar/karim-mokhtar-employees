import { useState } from "react";
import { modifyData } from "../utils";

const Employees = () => {
  const [data, setData] = useState([]);
  const handleChange = ev => {
    const reader = new FileReader();
    reader.onload = e => {
      const receivedData = e.target.result;
      // splitting each line of the file
      const records = receivedData.split("\n");

      //   modefy data to fit the ui
      const modefiedData = modifyData(records);

      // draw the output to the console
      console.table(modefiedData);
      setData(modefiedData);
    };
    reader.readAsText(ev.target.files[0]);
  };
  return (
    <div>
      <input onChange={handleChange} type="file" name="employees" />
      {data.length ? (
        <table>
          <tr>
            <th>Employee ID #1</th>
            <th>Employee ID #2</th>
            <th>Project ID</th>
            <th>Days worked</th>
          </tr>
          {data.map(ele => (
            <tr key={ele[3]}>
              <td>{ele[0]}</td>
              <td>{ele[1]}</td>
              <td>{ele[2]}</td>
              <td>{ele[3]}</td>
            </tr>
          ))}
        </table>
      ) : (
        <div>No match yet</div>
      )}
    </div>
  );
};

export default Employees;
