import "./App.css";
import Employees from "./components/Employees";

function App() {
  return (
    <div className="App">
      <h1>Pair Employees</h1>
      <Employees />
    </div>
  );
}

export default App;
